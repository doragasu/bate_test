# bate\_test

Test for the [Bot API for Telegram on ESP (BATE)](https://gitlab.com/doragasu/bate). This project implements a simple Telegram bot that echoes text and allows taking photographs on the ESP32-CAM module.

# Usage

1. Install [esp-idf](https://github.com/espressif/esp-idf)
2. Clone this repository including the submodules:

`$ git clone --recursive https://gitlab.com/doragasu/bate.git`

3. Run `make menuconfig` and configure the items inside `Test configuration`. Under `Component config`, configure `Telegram bot` and `Camera configuration`. Also make sure that `ESP32-specific ---> Support for external, SPI-connected RAM` is enabled and properly configured.
4. Connect your ESP32-CAM module to the PC, build, flash and monitor:

`$ make flash monitor`.

5. You can start the bot from your favorite client, sending `/start` command. `/photo` takes a photograph. Maybe there is some other funny command there to try...

# License

This project is licensed under the Mozilla Public License (MPL) 2.0. You can browse the license [here](https://www.mozilla.org/en-US/MPL/2.0/).
