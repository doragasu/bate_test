/************************************************************************//**
 * /brief Test program for the BATE Telegram client.
 *
 * Creates a simple bot that echoes text and allows taking photographs.
 *
 * \author Jesús Alonso (@doragasu)
 * \date 2019
 ****************************************************************************/
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <nvs_flash.h>
#include <esp_camera.h>
#include <string.h>

#include "app_wifi.h"
#include "app_camera.h"
#include "util.h"
#include "bate.h"

// Number of client errors to reach before desisting for a time interval
#define TG_ERROR_MAX 5
// Number of MS to wait before retry if TG_ERROR_MAX errors are reached
#define TG_ERROR_DELAY_MS 300000

static struct bate_bot_info *bot_get(void)
{
	cJSON *json = NULL;
	struct bate_bot_info *bot = NULL;

	bate_me_get(&json);
	bot = bate_me_parse(json);
	cJSON_Delete(json);

	return bot;
}

static void photo_send(long chat_id)
{
	camera_fb_t *photo;

	photo = esp_camera_fb_get();
	if (photo) {
		bate_file_send(CONFIG_TELEGRAM_TEST_CHAT_ID,
				(const char*)photo->buf, photo->len,
				"photo.jpg", BATE_FILE_PHOTO,
				BATE_MIME_IMAGE_JPG, NULL);
		esp_camera_fb_return(photo);
	}
}

static void start_reply(const char *chat_id)
{
	const char * const text = "*Welcome!*\nI am a Telegram bot running on "
		"an ESP32-CAM. I can take photographs and will echo anything I "
		"do not understand, just for testing purposes. Feel free to "
		"write anything  you want. You can browse the bot sources at "
		"[the bate_test repository]"
		"(https://gitlab.com/doragasu/bate_test).";
	const char *keyb[] = {"/photo", "/duck"};
	const unsigned int cols[] = {2};

	bate_keyboard_reply(chat_id, text, BATE_TEXT_MARKDOWN, keyb,
			1, cols, BATE_KEYB_RESIZE, NULL);
}

static void duck_reply(const char *chat_id)
{
	const char * const text = "Cuack!";
	const struct bate_inline_keyboard_button keyb[] = {
		{
			.type = BATE_INLINE_KEYB_TYPE_URL,
			.text = "Search",
			.url = "https://duck.com"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Callback",
			.callback_data = "quack_callback"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_SWITCH_INLINE_QUERY,
			.text = "Switch inline query",
			.switch_inline_query = "quack_query_data"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_SWITCH_INLINE_QUERY_CURRENT_CHAT,
			.text = "Inline query current chat",
			.switch_inline_query_current_chat = "quack_current_chat"
		}
	};
	const unsigned int cols[] = {1, 1, 1, 1};

	bate_inline_keyboard_reply(chat_id, text, 0, keyb, 4, cols, NULL);
}

static void not_allowed(const char *chat_id, const struct bate_message *msg)
{
	char *warn = NULL;
	struct bate_from *from = NULL;

	bate_msg_send(chat_id, "Nice try, but you are not "
			"allowed to use this bot.", 0, NULL);

	from = bate_from_parse(msg->from);
	if (msg->text) {
		asprintf(&warn, "%s wrote: %s", from->first_name, msg->text);
	} else {
		asprintf(&warn, "%s sent message without text",
				from->first_name);
	}
	bate_msg_send(CONFIG_TELEGRAM_TEST_CHAT_ID, warn,
			BATE_TEXT_PLAIN, NULL);

	bate_from_free(from);
	free(warn);
}

static void message_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	if (msg && msg->text) {
		if (!strcmp(msg->text, "/start")) {
			start_reply(chat_id);
		} else if (!strcmp(msg->text, "/photo")) {
			photo_send(chat->id);
		} else if (!strcmp(msg->text, "/duck")) {
			duck_reply(chat_id);
		} else {
			bate_msg_send(chat_id, msg->text,
					BATE_TEXT_PLAIN, NULL);
		}
	} else {
		// No supported command, echo message
		bate_msg_send(chat_id, msg->text, BATE_TEXT_PLAIN, NULL);
	}
}

static void message_json_parse(const cJSON *msg_json)
{

	char chat_id[32];
	struct bate_message *msg = bate_message_parse(msg_json);
	struct bate_chat *chat = bate_chat_parse(msg->chat);

	snprintf(chat_id, 32, "%ld", chat->id);

	if (strcmp(chat_id, CONFIG_TELEGRAM_TEST_CHAT_ID)) {
		// Message from client not allowed to use the bot
		not_allowed(chat_id, msg);
	} else {
		// Message from the client allowed to use the bot
		message_parse(chat_id, msg, chat);
	}

	bate_message_free(msg);
	bate_chat_free(chat);
}

static void callback_query_json_parse(const cJSON *cbq_json)
{
	struct bate_callback_query *query =
		bate_callback_query_parse(cbq_json);

	bate_callback_query_answer(query->id, "Got it!", false, NULL, 0, NULL);
	bate_callback_query_free(query);
}

static void unsupported_update_parse(void)
{
	LOGI("received unsupported update");
}

static long process_update(cJSON *update)
{
	long next;
	struct bate_update *up = bate_update_parse(update);
	LOGI("update_id: %ld", up->update_id);

	switch (up->type) {
	case BATE_UPDATE_TYPE_MESSAGE:
		message_json_parse(up->message);
		break;

	case BATE_UPDATE_TYPE_CALLBACK_QUERY:
		callback_query_json_parse(up->callback_query);
		break;

	default:
		unsupported_update_parse();
		break;
	}

	next = up->update_id + 1;
	bate_update_free(up);

	return next;
}

static void telebot_task(void *pvParameters)
{
	struct bate_bot_info *bot = NULL;
	long next_update = 0;
	int errors;
	cJSON *root, *updates, *update;

	app_wifi_wait_connected();
	LOGI("connected to AP, bot start");
	while (!(bot = bot_get())) {
		vTaskDelay(pdMS_TO_TICKS(60000/portTICK_RATE_MS));
	}

	LOGI("connected: id = %ld, first_name = %s, username = %s", bot->id,
			bot->first_name, bot->username);

	bate_msg_send(CONFIG_TELEGRAM_TEST_CHAT_ID,
			"Ready to serve!", 0, NULL);

	LOGI("Entering update loop");
	while (1) {
		errors = 0;
		while (errors < TG_ERROR_MAX) {
			if ((200 == bate_updates_get(next_update, 30, &root))) {
				updates = json_get_array(root, "result");
				cJSON_ArrayForEach(update, updates) {
					next_update = process_update(update);
				}
				cJSON_Delete(root);
				errors = 0;
			} else {
				errors++;
			}
		}
		LOGW("Connection error, sleeping %d s", TG_ERROR_DELAY_MS / 1000);
		vTaskDelay(pdMS_TO_TICKS(TG_ERROR_DELAY_MS));
	}
}

void app_main()
{
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);
	app_wifi_initialise();
	app_camera_main();

	xTaskCreate(&telebot_task, "telebot_task", 8192, NULL, 5, NULL);
}

